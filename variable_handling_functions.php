<?php
$floatvalue_with_string="1234.36str9bitm";
$only_float_value=floatval($floatvalue_with_string);
echo $only_float_value;


?>

<br>
//Testing empty function
<?php
$var1="";
$var2="hello";
echo "<br>";
var_dump(empty($var1));
echo "<br>";
var_dump(empty($var2));
?>


<br>
//Testing is_array function
<?php
$cars=array("Toyota","BMW","Volvo");
$x="This is a string";
echo "<br>";
var_dump(is_array($cars));
echo "<br>";
var_dump(is_array($x));
echo "<br>";
?>


//Testing is_null function
<?php
$x=NULL;
$y=0;
var_dump(is_null($x));
echo "<br>";
var_dump(is_null($y));
echo "<br>";
var_dump(is_null($z));
echo "<br>";
?>


//Testing is_object function
<?php
$obj1=(object)array('name'=>'rumpa','class'=>'5');
$obj2=array("jayna","Nawfa","Farha");
echo "<br>";
var_dump(is_object($obj1));
echo "<br>";
var_dump(is_object($obj2));
echo "<br>";
?>


//Testing serialize and unserialize function
<?php
echo "<br>";
$arr=array("car","bus","rickshaw");
$str=serialize($arr);
echo $str;
echo "<br>";
$newstr=unserialize($str);
print_r($newstr);
echo "<br>";
?>

//Testing isset function
<?php
$x=NULL;
$y="hello";
$z="BITM";
echo "<br>";
var_dump(isset($x));
echo "<br>";
var_dump(isset($y));
echo "<br>";
var_dump(isset($z));
echo "<br>";
var_dump(isset($m));
?>



<br>
//Testing print_r function
<pre>
<?php
$a=array('a'=>'apple','b'=>'banana','c'=>array('x','y','z'));
print_r($a);
?>
</pre>


<br>
//Testing unset function
<?php
$name="Rumpa";
echo "<br>$name";
$newval=(unset)$name;
echo $newval."<br>";
var_dump($name);
echo "<br>";
var_dump((unset)$name);
echo "<br>";
?>

//Testing var_dump function
<?php
$a=3.1;
$b="hello";
$c=true;
$d=null;
echo "<br>";
var_dump($a);
echo "<br>";
var_dump($b);
echo "<br>";
var_dump($c);
echo "<br>";
var_dump($d);
echo "<br>";

$arr=array(1,2,array("a","b","c"));
var_dump($arr);
echo "<br>";

?>


//Testing var_export function
<?php
echo "<br>";
$a=3.1;
var_export($a);

echo "<br>";
$arr=array(1,2,array("a","b","c"));
var_export($arr);
echo "<br>";

?>

//Testing gettype function
<?php
echo "<br>";
$data=array('Float',34.56,null,false);
foreach ($data as $value)
{
    echo gettype($value)."<br>";
}
?>


//Testing is_bool function
<?php
echo "<br>";
$a=false;
$b=0;
echo is_bool($a)."<br>";
echo is_bool($b)."<br>";
if (is_bool($a)===true)
{
    echo "Yes,this is a Boolen.";
}
echo "<br>";
if(is_bool($b===false))
{
    echo "No, it is not a Boolean";
}

?>
<br>
//Testing is_float function
<?php
echo "<br>";
$a=27.25;
$b="abc";
$c=23;
echo is_float($a)."<br>";
echo is_float($b)."<br>";
echo is_float($c)."<br>";
if (is_float($b))
{
    echo "Yes,this is a Float.";
}
else
{
    echo "This is not float";
}
echo "<br>";
var_dump(is_float($a));
echo "<br>";
var_dump(is_float($b));
echo "<br>";
var_dump(is_float($c));
echo "<br>";
?>


//Testing is_string function
<?php
echo "<br>";
$a=27.25;
$b="abc";
$c=23;
echo is_string($a)."<br>";
echo is_string($b)."<br>";
echo is_string($c)."<br>";
if (is_string($a))
{
    echo "Yes,this is a String.";
}
else
{
    echo "This is not String";
}
echo "<br>";
var_dump(is_string($a));
echo "<br>";
var_dump(is_string($b));
echo "<br>";
var_dump(is_string($c));
echo "<br>";
?>


//Testing is_int function
<?php
$data=array(23,"23",23.5,"23.5",null,true,false);
echo "<br>";
foreach($data as $value)
{
    //echo "<br>".is_int($value);

    var_export(is_int($value));
    echo "<br>";

}
foreach($data as $value)
{
    //echo "<br>".is_int($value);

    var_dump(is_int($value));
    echo "<br>";

}
?>





